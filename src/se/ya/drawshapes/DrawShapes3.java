package se.ya.drawshapes;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

public class DrawShapes3 extends Application {
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(300, 250);
        DrawContext dc = new DrawContext(canvas.getGraphicsContext2D());
        
        ArrayList<Shape> shapes = CreateShapes();
        
        drawShapes(dc, shapes);
        moveShapes(shapes, 5, 100);
        drawShapes(dc, shapes);
        
        Group root = new Group();
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private ArrayList<Shape> CreateShapes() {
        ArrayList<Shape> shapes = new ArrayList<>();
        
        shapes.add(CreateRectangle(120, 30, 60, 40));
        shapes.add(CreateTriangle(230, 20, 40, 60));
        shapes.add(CreateSquare(20, 20, 60));
        
        return shapes;
    }
    
    private Shape CreateRectangle(int x, int y, int width, int height) {
        return new Rectangle(x, y, width, height);
    }
    
    private Shape CreateSquare(int x, int y, int side) {
        return new Square(x, y, side);
    }
    
    private Shape CreateTriangle(int x, int y, int width, int height) {
        return new Triangle(x, y, width, height);
    }

    private void drawShapes(DrawContext dc, ArrayList<Shape> shapes) {
        for (Shape shape : shapes) {
            dc.moveTo(shape.x, shape.y);
            dc.penDown();
            
            if (shape instanceof Rectangle) {
                Rectangle rectangle = (Rectangle)shape;
                
                dc.moveTo(rectangle.x + rectangle.width, rectangle.y);
                dc.moveTo(rectangle.x + rectangle.width, rectangle.y + rectangle.height);
                dc.moveTo(rectangle.x, rectangle.y + rectangle.height);
                dc.moveTo(rectangle.x, rectangle.y);
            }
            else if (shape instanceof Square) {
                Square square = (Square)shape;
                
                dc.moveTo(square.x + square.side, square.y);
                dc.moveTo(square.x + square.side, square.y + square.side);
                dc.moveTo(square.x, square.y + square.side);
                dc.moveTo(square.x, square.y);
            }
            else if (shape instanceof Triangle) {
                Triangle triangle = (Triangle)shape;
                
                dc.moveTo(triangle.x + triangle.width, triangle.y);
                dc.moveTo(triangle.x + triangle.width, triangle.y + triangle.height);
                dc.moveTo(triangle.x, triangle.y);
            }
            
            dc.penUp();
        }
    }

    private void moveShapes(ArrayList<Shape> shapes, int dx, int dy) {
        for (Shape shape : shapes) {
            shape.x += dx;
            shape.y += dy;
        }
    }

    abstract class Shape {
        public int x;
        public int y;
        
        public Shape(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    
    class Rectangle extends Shape {
        public int width;
        public int height;
        
        public Rectangle(int x, int y, int width, int height) {
            super(x, y);
            
            this.width = width;
            this.height = height;
        }
    }

    class Square extends Shape {
        public int side;
        
        public Square(int x, int y, int side) {
            super(x, y);
            
            this.side = side;
        }
    }

    class Triangle extends Shape {
        public int width;
        public int height;
        
        public Triangle(int x, int y, int width, int height) {
            super(x, y);
            
            this.width = width;
            this.height = height;
        }
    }
}
