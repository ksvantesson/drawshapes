package se.ya.drawshapes;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

public class DrawShapes1 extends Application {
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(300, 250);
        DrawContext dc = new DrawContext(canvas.getGraphicsContext2D());
        
        ArrayList<Omnishape> shapes = CreateShapes();
        
        drawShapes(dc, shapes);
        moveShapes(shapes, 5, 100);
        drawShapes(dc, shapes);
        
        Group root = new Group();
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private ArrayList<Omnishape> CreateShapes() {
        ArrayList<Omnishape> shapes = new ArrayList<>();
        
        shapes.add(CreateSquare(20, 20, 60));
        shapes.add(CreateRectangle(120, 30, 60, 40));
        shapes.add(CreateTriangle(230, 20, 40, 60));
        
        return shapes;
    }
    
    private Omnishape CreateRectangle(int x, int y, int width, int height) {
        Omnishape rectangle = new Omnishape();
        
        rectangle.x = x;
        rectangle.y = y;
        rectangle.width = width;
        rectangle.height = height;
        rectangle.isRectangle = true;
        
        return rectangle;
    }
    
    private Omnishape CreateSquare(int x, int y, int side) {
        Omnishape square = new Omnishape();
        
        square.x = x;
        square.y = y;
        square.side = side;
        square.isSquare = true;
        
        return square;
    }
    
    private Omnishape CreateTriangle(int x, int y, int width, int height) {
        Omnishape triangle = new Omnishape();
        
        triangle.x = x;
        triangle.y = y;
        triangle.width = width;
        triangle.height = height;
        triangle.isTriangle = true;
        
        return triangle;
    }

    private void drawShapes(DrawContext dc, ArrayList<Omnishape> shapes) {
        for (Omnishape shape : shapes) {
            if (shape.isRectangle) {
                dc.moveTo(shape.x, shape.y);
                dc.penDown();
                dc.moveTo(shape.x + shape.width, shape.y);
                dc.moveTo(shape.x + shape.width, shape.y + shape.height);
                dc.moveTo(shape.x, shape.y + shape.height);
                dc.moveTo(shape.x, shape.y);
                dc.penUp();
            }
            else if (shape.isSquare) {
                dc.moveTo(shape.x, shape.y);
                dc.penDown();
                dc.moveTo(shape.x + shape.side, shape.y);
                dc.moveTo(shape.x + shape.side, shape.y + shape.side);
                dc.moveTo(shape.x, shape.y + shape.side);
                dc.moveTo(shape.x, shape.y);
                dc.penUp();
            }
            else if (shape.isTriangle) {
                dc.moveTo(shape.x, shape.y);
                dc.penDown();
                dc.moveTo(shape.x + shape.width, shape.y);
                dc.moveTo(shape.x + shape.width, shape.y + shape.height);
                dc.moveTo(shape.x, shape.y);
                dc.penUp();
            }
        }
    }

    private void moveShapes(ArrayList<Omnishape> shapes, int dx, int dy) {
        for (Omnishape shape : shapes) {
            shape.x += dx;
            shape.y += dy;
        }
    }

    class Omnishape {
        public int x;
        public int y;
        public boolean isRectangle;
        public int width; // Rectangle's width
        public int height; // Rectangle's height
        public boolean isSquare;
        public int side; // Square's side
        public boolean isTriangle; // Triangle uses width and height too
    }
}
