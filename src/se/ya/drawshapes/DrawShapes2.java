package se.ya.drawshapes;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

public class DrawShapes2 extends Application {
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(300, 250);
        DrawContext dc = new DrawContext(canvas.getGraphicsContext2D());
        
        ArrayList<Object> shapes = CreateShapes();
        
        drawShapes(dc, shapes);
        moveShapes(shapes, 5, 100);
        drawShapes(dc, shapes);
        
        Group root = new Group();
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private ArrayList<Object> CreateShapes() {
        ArrayList<Object> shapes = new ArrayList<>();
        
        shapes.add(CreateTriangle(230, 20, 40, 60));
        shapes.add(CreateSquare(20, 20, 60));
        shapes.add(CreateRectangle(120, 30, 60, 40));
        
        return shapes;
    }
    
    private Object CreateRectangle(int x, int y, int width, int height) {
        return new Rectangle(x, y, width, height);
    }
    
    private Object CreateSquare(int x, int y, int side) {
        return new Square(x, y, side);
    }
    
    private Object CreateTriangle(int x, int y, int width, int height) {
        return new Triangle(x, y, width, height);
    }

    private void drawShapes(DrawContext dc, ArrayList<Object> shapes) {
        for (Object shape : shapes) {
            if (shape instanceof Rectangle) {
                Rectangle rectangle = (Rectangle)shape;
                
                dc.moveTo(rectangle.x, rectangle.y);
                dc.penDown();
                dc.moveTo(rectangle.x + rectangle.width, rectangle.y);
                dc.moveTo(rectangle.x + rectangle.width, rectangle.y + rectangle.height);
                dc.moveTo(rectangle.x, rectangle.y + rectangle.height);
                dc.moveTo(rectangle.x, rectangle.y);
                dc.penUp();
            }
            else if (shape instanceof Square) {
                Square square = (Square)shape;
                
                dc.moveTo(square.x, square.y);
                dc.penDown();
                dc.moveTo(square.x + square.side, square.y);
                dc.moveTo(square.x + square.side, square.y + square.side);
                dc.moveTo(square.x, square.y + square.side);
                dc.moveTo(square.x, square.y);
                dc.penUp();
            }
            else if (shape instanceof Triangle) {
                Triangle triangle = (Triangle)shape;
                
                dc.moveTo(triangle.x, triangle.y);
                dc.penDown();
                dc.moveTo(triangle.x + triangle.width, triangle.y);
                dc.moveTo(triangle.x + triangle.width, triangle.y + triangle.height);
                dc.moveTo(triangle.x, triangle.y);
                dc.penUp();
            }
        }
    }

    private void moveShapes(ArrayList<Object> shapes, int dx, int dy) {
        for (Object shape : shapes) {
            if (shape instanceof Rectangle) {
                Rectangle rectangle = (Rectangle)shape;
                
                rectangle.x += dx;
                rectangle.y += dy;
            }
            else if (shape instanceof Square) {
                Square square = (Square)shape;
                
                square.x += dx;
                square.y += dy;
            }
            else if (shape instanceof Triangle) {
                Triangle triangle = (Triangle)shape;
                
                triangle.x += dx;
                triangle.y += dy;
            }
        }
    }

    class Rectangle {
        public int x;
        public int y;
        public int width;
        public int height;
        
        public Rectangle(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }

    class Square {
        public int x;
        public int y;
        public int side;
        
        public Square(int x, int y, int side) {
            this.x = x;
            this.y = y;
            this.side = side;
        }
    }

    class Triangle {
        public int x;
        public int y;
        public int width;
        public int height;
        
        public Triangle(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }
}
