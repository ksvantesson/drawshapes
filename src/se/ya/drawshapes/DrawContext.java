package se.ya.drawshapes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class DrawContext {
    private GraphicsContext gc;
    private boolean isDrawing;
    private int lastX;
    private int lastY;

    public DrawContext(GraphicsContext gc) {
        this.gc = gc;
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
    }
    
    public void penDown() {
        isDrawing = true;
    }

    public void penUp() {
        isDrawing = false;
    }
    
    public void moveTo(int x, int y) {
        if (isDrawing)
            gc.strokeLine(lastX, lastY, x, y);
        
        lastX = x;
        lastY = y;        
    }
}
