package se.ya.drawshapes;

import java.util.ArrayList;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;
import static javafx.application.Application.launch;

public class DrawShapes4 extends Application {
    public static void main(String[] args) {
        launch(args);
    }
 
    @Override
    public void start(Stage primaryStage) {
        Canvas canvas = new Canvas(300, 250);
        DrawContext dc = new DrawContext(canvas.getGraphicsContext2D());
        
        ArrayList<Shape> shapes = CreateShapes();
        
        drawShapes(dc, shapes);
        moveShapes(shapes, 5, 100);
        drawShapes(dc, shapes);
        
        Group root = new Group();
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private ArrayList<Shape> CreateShapes() {
        ArrayList<Shape> shapes = new ArrayList<>();
        
        shapes.add(CreateRectangle(120, 30, 60, 40));
        shapes.add(CreateTriangle(230, 20, 40, 60));
        shapes.add(CreateSquare(20, 20, 60));
        
        return shapes;
    }
    
    private Shape CreateRectangle(int x, int y, int width, int height) {
        return new Rectangle(x, y, width, height);
    }
    
    private Shape CreateSquare(int x, int y, int side) {
        return new Square(x, y, side);
    }
    
    private Shape CreateTriangle(int x, int y, int width, int height) {
        return new Triangle(x, y, width, height);
    }

    private void drawShapes(DrawContext dc, ArrayList<Shape> shapes) {
        for (Shape shape : shapes) {
            shape.draw(dc);
        }
    }

    private void moveShapes(ArrayList<Shape> shapes, int dx, int dy) {
        for (Shape shape : shapes) {
            shape.move(dx, dy);
        }
    }

    abstract class Shape {
        protected int x;
        protected int y;
        
        public Shape(int x, int y) {
            this.x = x;
            this.y = y;
        }
        
        public void move(int dx, int dy) {
            x += dx;
            y += dy;
        }
        
        public void draw(DrawContext dc) {
            dc.penUp();
            dc.moveTo(x, y);
            dc.penDown();
        }
    }
    
    class Rectangle extends Shape {
        private int width;
        private int height;
        
        public Rectangle(int x, int y, int width, int height) {
            super(x, y);
            
            this.width = width;
            this.height = height;
        }
        
        @Override
        public void draw(DrawContext dc) {
            super.draw(dc);
            
            dc.moveTo(x + width, y);
            dc.moveTo(x + width, y + height);
            dc.moveTo(x, y + height);
            dc.moveTo(x, y);
        }
    }

    class Square extends Shape {
        private int side;
        
        public Square(int x, int y, int side) {
            super(x, y);
            
            this.side = side;
        }
        
        @Override
        public void draw(DrawContext dc) {
            super.draw(dc);
            
            dc.moveTo(x + side, y);
            dc.moveTo(x + side, y + side);
            dc.moveTo(x, y + side);
            dc.moveTo(x, y);
        }
    }

    class Triangle extends Shape {
        private int width;
        private int height;
        
        public Triangle(int x, int y, int width, int height) {
            super(x, y);
            
            this.width = width;
            this.height = height;
        }
        
        @Override
        public void draw(DrawContext dc) {
            super.draw(dc);
            
            dc.moveTo(x + width, y);
            dc.moveTo(x + width, y + height);
            dc.moveTo(x, y);
        }
    }
}
